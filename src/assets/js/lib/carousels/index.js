import $ from 'jquery'
import 'slick-carousel'

export default () => {
	$("#staff-slider").slick({

	  slidesToShow: 2,
	  slidesToScroll: 2,
	  autoplay: false,
	  arrows: true,
	  prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
	  nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
	  swipeToSlide: true,
	  infinite: true,
	  dots: true,
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 1
	      }
	    }
	  ]
	});
};
