import $ from 'jquery'
import googleMapsLoader from 'load-google-maps-api'
//

export const apiKey = 'AIzaSyAmjOz9vdCVJTyNqZMKTjTVelFH30oD8f0'

export const defaultMinHeight = 250
export const defaultHeight = '100%'

const ensureElementHasHeight = (
  $el,
  minHeight = defaultMinHeight,
  height = defaultHeight
) => {
  if ($el.height() <= 0) {
    $el.css({ minHeight, height })
  }
}

const readOptionsFromData = $el => {
  const lat = +$el.data('map-lat')
  const lng = +$el.data('map-lng')
  const zoom = +$el.data('map-zoom')

  if (!lat) {
    throw new Error('Please add a "data-map-lat" attribute with the latitude.')
  } else if (!lng) {
    throw new Error('Please add a "data-map-lng" attribute with the longitude.')
  } else if (!zoom) {
    throw new Error(
      'Please add a "data-map-zoom" attribute with the map zoom level.'
    )
  }

  return {
    center: {
      lat,
      lng,
    },
    zoom,
  }
}

export const googleMaps = stylePredicate => {
  $(document).ready(() => {
    const loader = googleMapsLoader({ key: apiKey })

    $('[data-map]').each((index, el) => {
      const $el = $(el)

      let styles

      if (typeof stylePredicate === 'function') {
        styles = stylePredicate($el.data('map-style'))
      }

      ensureElementHasHeight($el)

      const options = { ...readOptionsFromData($el), ...{ styles } }

      $el.each((index, el) => {
        const $el = $(el)

        loader.then(google => {
          const map = new google.Map(el, options)

          const markerOptions = {
            position: options.center,
            map,
            icon: $el.data('map-marker-url') || null,
            animation: google.Animation.DROP,
          }


          const marker = new google.Marker({ position: options.center, map })

          const infoWindow = new google.InfoWindow({
            content: `
<div class="map--info">
  <h3>
    The George Washington University Hospital
  </h3>
  <p>
    900 23rd Street, NW <br>
    Washington, DC 20037
  </p>
  <a href="https://www.google.com/maps/dir//George+Washington+University+Hospital,+900+23rd+St+NW,+Washington,+DC+20037/@38.901261,-77.0529352,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x89b7b7b3d8f47f3b:0x2139fc5175afdc65!2m2!1d-77.0507412!2d38.901261" target="_blank" class="sl_button sl_button--primary">Get Directions</a>
</div>
  `,
          })

          marker.addListener('click', () => {
            infoWindow.open(map, marker)
          })
        })
      })
    })
  })
}

export default googleMaps
